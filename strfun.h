#ifndef STRFUN_H
#define STRFUN_H

/*

COPYRIGHT 2015 (c) Thomas Kendall
Data Structure and Algorithms
Autumn Semester - City Campus


Strfun is short for string functions. This supplements
the functions in the std::string library with functions
such as char to string conversions, checking if a string
contains another string etc. See the cpp file for
function specific comments.

*/


#include <iostream>
#include <sstream>
#include <string>
#include <algorithm>

using namespace std;

class strfun {

   public:
   static string toLower(string input);
   static bool stringContains(string string1, string string2);
   static string charToString(char input);
   static string getLastChar(string input);
   static void removeCharacters(string *passedString, int *passedCount);
};

#endif
