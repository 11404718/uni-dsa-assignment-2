#include "strfun.h"

using namespace std;

// Convert character or string to lowercase, relies on <algorithm>.
string strfun::toLower(string input) {
   string output = input;
   transform(output.begin(), output.end(), output.begin(), ::tolower);

   return output;
}


// Tests if a string contains another string.
bool strfun::stringContains(string string1, string string2)
{
   return string1.find(string2) != string::npos;
}


// Converts a character to a string, used mainly for comparisons.
string strfun::charToString(char input) {
   stringstream stringConverter;
   string output;

   stringConverter << input;
   stringConverter >> output;
   
   return output;
}


// Get last character from string.
string strfun::getLastChar(string input) {
   return input.substr(input.size() - 1, 1);
}


// Remove irrelevant characters (particularly numbers) from
// the provided string (passed by reference), so that only
// words remain.
void strfun::removeCharacters(string *passedString, int *passedCount) {

   string input = *passedString;

   const string compare = ".0123456789,;:\"~!#%^*()=+[]{}\\|<>?/ ";
   string output;
   int wordCount = 0;

   for (string::iterator it = input.begin(), end = input.end(); it != end; it++)
   {

      string currentChar = strfun::charToString(*it);

      // If removable character.
      if (strfun::stringContains(compare, currentChar)) {

         // Only create a space (word separator) if there is no previous space.
         // Can use getLastChar as the string is being built by the loop and
         // the ending character will always be the "previous" character.
         if (strfun::getLastChar(output) != "\n") {
            output.append("\n");
            wordCount++;
         }

      }

      // Else if whitespace character.
      // Character treated as normal, appended. 
      else {

         output.append(strfun::toLower(currentChar));

      }

   }

   *passedCount = wordCount;
   *passedString = output;

}
