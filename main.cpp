#include <iostream>
#include <fstream>

#include "main.h"
#include "bintree.h"
#include "strfun.h"

using namespace std;

int main(int argc, char* argv[])
{
   // Check the user has provided the right number of filenames.
   if (argc != 3)
   {
      cout << "\nERROR - Incorrect number of arguments\n";
      cout << "Syntax : spellcheck dictionary filename\n";
      return 0;
   }
   
   // Declare variables and binary trees.
   int totalWordCount = 0, unknownSpellingCount = 0;
   bintree<string> dict;
   
   
   // Declare filenames.
   ifstream dictFile, textFile;
   string dictFileName = argv[1];
   string textFileName = argv[2];
   
   
   // Open files with filenames provided.
   dictFile.open(dictFileName.c_str());
   textFile.open(textFileName.c_str());
   
   
   // Check if dictionary file is valid, and if so place
   // correct words into dict binary tree.
   if (dictFile.is_open()) {
      string str;
      
      while (getline(dictFile, str))
      {
         dict.insert(str);
      }     
      
   }
   else
   {
      cout << "Program failed with the following error\n";
      cout << "Unable to open dictionary file to read\n";
   }
   
   
   string contents;
   
   // Check if text file is valid, and if so read
   // the file contents into contents var.
   if (textFile.is_open()) {
      
      // Read file contents into a string.
      string str;
      while (getline(textFile, str))
      {
         contents.append(str + "\n");
      }
      
      
      // Run removeCharacters to remove numbers, punctuation
      // characters etc.
      strfun::removeCharacters(&contents, &totalWordCount);
      
      
      // Check each "cleaned" word against the dictionary.
      string currentWord;
      for (string::iterator it = contents.begin(), end = contents.end(); it != end; it++)
      {
      
         if (*it != '\n') {
            // Continue building the word if a newline is not reached.
            currentWord.append(strfun::charToString(*it));
            
         // Word is complete, and can be tested against the dictionary.   
         } else {
            if (dict.find(currentWord) == NULL) {
               cout << currentWord << "\n";
               unknownSpellingCount++;
            }
            currentWord = "";
         }
      }
      
      
      // Output program findings.
      cout << "A total of " << totalWordCount << " words were found in the file\n";
      cout << unknownSpellingCount << " had unknown spelling";
      
   }
   else
   {
      cout << "Program failed with the following error\nUnable to access " << textFileName;
   }
   
   cout << "\n";
   
   return 0;
}
